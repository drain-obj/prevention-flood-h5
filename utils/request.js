  const baseURL='http://172.19.0.23:2222';
  const request=(params = {})=>{
    return new Promise((resolve, reject)=>{
      uni.showLoading({
        title: '玩命加载中...',
      })
      uni.request({
        url:baseURL + params.url,
        method: params.method || 'GET',
        data: params.data || "",
        header: {
			Authorization:uni.getStorageSync('token')
        },
        success: (res)=>{
          resolve(res);
        },
        fail: (err)=>{
          reject(err);
        },
        complete: () =>{
          uni.hideLoading();
        }
      })
    })
  }
  
 export default request