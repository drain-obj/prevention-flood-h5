import axios from "axios";
// import Cookies from "js-cookie";
import config from "./config";
import store from '../store'
//import router from "@/router";
// import {
//   MessageBox,
//   Message
// } from "element-ui";
//import QS from "qs";
console.log()
export default function $axios(options,configs) {
	if(configs && configs.headers){
		config.headers=configs.headers;
	}
  return new Promise((resolve, reject) => {
    const instance = axios.create({
      baseURL: config.baseUrl,
      headers: config.headers,
      timeout: config.timeout,
      withCredentials: config.withCredentials
    });

    // request 拦截器
    instance.interceptors.request.use(
      config => {
        //在发送之前做点什么
        if(store.state.userInfo.token){
          config.headers.token = store.state.userInfo.token;
        }else{
        }
        if (config.method === "post") {
        }
        return config;
      },
      error => {
        // 判断请求超时
        if (
          error.code === "ECONNABORTED" &&
          error.message.indexOf("timeout") !== -1
        ) {
          // Toast("信号不好，请求超时")
          // Message({
          //   message: "信号不好，请求超时",
          //   type: "error",
          //   duration: 5 * 1000
          // });
        }
      }
    );

    // response 拦截器
    instance.interceptors.response.use(
      response => {
        //对响应数据做点什么
        let data;
        if (response.data == undefined) {
          data = JSON.parse(response.request.responseText);
        } else {
          data = response.data;
        }
        return data;
      },
      err => {
        if (err && err.response) {
          if(err.response.status=='401'){
            uni.showToast({
              title: '登录过期，请重新登陆',
              icon:'none',
              duration: 1000
            });
            setTimeout(()=>{
              uni.navigateTo({
                url: '/pages/jumpPage/index'
            });
            },1000)
          }
          /*   console.log(err)*/
        } // 返回接口返回的错误信息
        return Promise.reject(err);
      }
    );

    // 请求处理
    instance(options)
      .then(res => {
        resolve(res);
        return false;
      })
      .catch(error => {
        reject(error);
      });
  });
}
