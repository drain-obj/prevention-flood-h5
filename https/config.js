//佛山现场  manifest.json的运行基础路径设为  /drainage
// const baseUrl =  "/drainage/drain";

// 本地测试
// const baseUrl = "http://"+ window.location.hostname +":7171"
//const baseUrl = "http://10.31.204.29:2222"
const baseUrl = "/floodApp"
// const baseUrl = "http://172.19.0.64:2222"
// const baseUrl = "http://172.19.0.14:2222"
// const baseUrl = "http://172.19.2.68:2222"
// const baseUrl ="http://10.5.4.80:2222"


export default {
  method: "get",
  // 基础url前缀`
  baseUrl: baseUrl, //请求的域名地址
  // 请求头信息
  headers: {
    "Content-Type": "application/json;charset=UTF-8"
  },
  // 参数
  data: {},
  // 设置超时时间
  timeout: 10000 * 20,
  // 携带凭证
  withCredentials: false,
  // 返回数据类型
  responseType: "json"
  
};
