import axios from "../axios";
import qs from "qs";
import config from "../config.js";
import axiosDel from "axios";

let customUrl = {
	MONITOR_RAIN:"/mobile/api/monitor/line/rain",  //降水
	MONITOR_REO:"/mobile/api/monitor/line/reservoir", //水库
	MONITOR_PIPE:"/mobile/api/monitor/line/pipe",  //管网
	MONITOR_RIVER:"/mobile/api/monitor/line/river", //河道
	MONITOR_WATER:"/mobile/api/monitor/line/seeper"  //积水
};

//监测--通用 --曲线
export const lineCustom = json => {
	return axios({
		url: customUrl[json.type]+`?code=${json.code}&sd=${json.sd}&ed=${json.ed}`,
		method: "get"
	});
};

//监测--降水
export const monitorRain = json => {
	return axios({
		url: `/mobile/api/monitor/rain`,
		// url: `/equip/monitorAlarm/getRainAlarmList`,
		method: "get"
	});
};

//监测--来水
export const monitorSw = json => {
	return axios({
		url: `/mobile/api/monitor/sw`,
		method: "get"
	});
};

//监测--来水 --监测曲线
export const lineSw = json => {
	return axios({
		url: `/mobile/api/monitor/line/lsv1?code=${json.code}`,
		method: "get"
	});
};

//监测--河道
export const monitorRiver = json => {
	return axios({
		url: `/mobile/api/monitor/river`,
		method: "get"
	});
};

//监测--水库
export const monitorRreservoir = json => {
	return axios({
		url: `/mobile/api/monitor/reservoir`,
		method: "get"
	});
};
//监测--管网
export const monitorPipe = json => {
	return axios({
		url: `/mobile/api/monitor/pipe`,
		method: "get"
	});
};
//监测--积水
export const monitorWater = json => {
	return axios({
		url: `/mobile/api/monitor/water`,
		method: "get"
	});
};

//一张图 积水险情列表 参数 type 1积水险情  2工程险情 3应急物资 4视频查看
export const getJSAndGCList = json => {
	return axios({
		url: `/mobile/api/dispatch/task/getJSAndGCList?type=${json.type}`,
		method: "get"
	});
};

//任务管理 详情  
export const taskDetail = json => {
	return axios({
		url: `/mobile/api/task/detail?id=${json.id}`,
		method: "get"
	});
};


//险情上报
export const reportSave = json => {
	return axios({
		// url: `/mobile/api/report/save`,
		url:`/dispatch/creatNewTask`,
		method: "post",
		data: json,
		headers: {
		   "Content-Type": "multipart/form-data",
	    },
	});
};

//会商 列表  
export const meetListAll = (id) => {
	return axios({
		url: `/dispatch/meet/getListAll?flag=1&id=${id}`,
		method: "get"
	});
};

//会商 人员信息
export const userListAll = (params) => {
	return axios({
		url: `/system/user/getListAll?param=${params||''}`,
		method: "get"
	});
};
//账号 人员信息
export const getUserInfo = json => {
	return axios({
		url: `/system/user/getCurrentUserInfo`,
		method: "get"
	});
};

//会商 创建会商接口
export const meetEdit = json => {
	return axios({
		url: `/dispatch/meet/edit`,
		method: "post",
		data: json,
	});
};

//会商 添加人员接口
export const editMeetUser = json => {
	return axios({
		url: `/dispatch/meet/editMeetUser`,
		method: "post",
		data: json,
	});
};

//会商 进行中的聊天记录
export const getZTMeetListMessage = json => {
	return axios({
		url: `/dispatch/meet/getZTMeetListMessage?meetId=${json.meetId}`,
		method: "get",
	});
};

//会商 已结束的聊天记录 
export const getMeetListMessage = json => {
	return axios({
		url: `/dispatch/meet/getMeetListMessage?meetId=${json.meetId}`,
		method: "get",
	});
};

//GET 获取当前会商的所有人员
export const getMeetUserList = json => {
	return axios({
		url: `/dispatch/meet/getMeetUserList?meetId=${json.meetId}`,
		method: "get",
	});
};

//GET 结束当前会商
export const stopMeet = json => {
	return axios({
		url: `/dispatch/meet/stopMeet?meetId=${json.meetId}`,
		method: "get",
	});
};

export const taskList = json =>{
	return axios({
		url: `/mobile/api/task/list/all?id=${json.id}&keyword=${json.keyword}`,
		method: "get",
	});
};

export const assignToMeTaskList = json =>{
	return axios({
		url: `/mobile/api/task/list/mine?id=${json.id}&user=${json.user}&keyword=${json.keyword}`,
		method: "get",
	});
};

export const myAssignTaskList = json =>{
	return axios({
		url: `/mobile/api/task/list/assign?id=${json.id}&user=${json.user}&keyword=${json.keyword}`,
		method: "get",
	});
};

export const initedTaskList = json =>{
	return axios({
		url: `/mobile/api/task/list/init?id=${json.id}&keyword=${json.keyword}`,
		method: "get",
	});
};

//任务处置 提交
export const handleTask = json => {
	return axios({
		url: `/dispatch/handleTask`,
		method: "post",
		data: json,
		headers: {
		   "Content-Type": "multipart/form-data",
		},
	});
};


//处置 人员转移 安置地点接口
export const placeGetAll = json => {
	return axios({
		url: `/emergency/place/getAll`,
		method: "post"
	});
};


//处置 力量调度 防汛队伍接口
export const teamGetAll = json => {
	return axios({
		url: `/emergency/team/all`,
	});
};
//事件列表
export const eventList = json => {
	return axios({
		url: `/dispatch/incident/getList`,

		method: "get"
	});
};


//处置 力量调度 防汛车辆接口
export const carGetAll = json => {
	return axios({
		url: `/emergency/car/all`,
		method: "post"
	});
};


//处置 物资调度 仓库信息接口
export const storeHouseMaterial = json => {
	return axios({
		url: `/emergency/storeHouseMaterial`,
		method: "get"
	});
};
// 街道列表
export const streettList = json => {
	return axios({
		url: `/common/combox/street`,
		method: "get"
	});
};

// 灾情上报
export const saveDisaster = json => {
	return axios({
		url:`/dispatch/saveDispatchIncidentAssess`,
		method: "post",
		data: json,
	});
};

// 灾情上报列表接口
export const getDispatchIncidentAssess = (id) => {
	return axios({
		url: `/dispatch/getDispatchIncidentAssess?id=${id}`,
		method: "get"
	});
};

// 灾情历史记录查看接口
export const getIncidentAssessDetails = json => {
	return axios({
		url: `/dispatch/getIncidentAssessDetails?incidentId=${json.incidentId}&streetId=${json.streetId}`,
		method: "get"
	});
};

//资源调度上报 归属事件接口
export const getOverIncidentName = json => {
	return axios({
		url: `/dispatch/getOverIncidentName`,
		method: "get"
	});
};

//资源调度上报 街道接口
export const street = json => {
	return axios({
		url: `/common/combox/street`,
		method: "get"
	});
};

//资源调度上报 物资下拉框
export const getMaterialAll = json => {
	return axios({
		url: `/emergency/material/getMaterialAll`,
		method: "get"
	});
};

//资源调度上报  上报接口
export const saveIncidentMaterial = json => {
	return axios({
		url: `/dispatch/saveIncidentMaterial`,
		method: "post",
		data: json,
	});
};

//资源调度上报  历史记录列表接口
export const getIncidentMaterialList = (id) => {
	return axios({
		url: `/dispatch/getIncidentMaterialList?id=${id}`,
		method: "get"
	});
};


// 获取当前事件 街道对应的内容
export const getIncidentMaterial = json => {
	return axios({
		url: `/dispatch/getIncidentMaterial?incidentId=${json.incidentId}&streetId=${json.streetId}`,
		method: "get"
	});
};

// 转派，指派
export const dispatchTask = json => {
	return axios({
		url: `/dispatch/dispatchTask?taskId=${json.taskId}&userId=${json.userId}&finalTime=${json.finalTime}`,
		method: "get"
	});
};
// 获取token
export const getToken = token => {
	return axios({
		url: `/mobile/token/getUserInfo?token=${token}`,
		method: "get"
	});
};
// 人员转移
export const dispatchReport = json => {
	return axios({
		url: `/dispatch/savePersonnelTransferReport`,
		method: "post",
		data: json,
	});
};
// 应急仓库查询
export const getWarehouse = json => {
	return axios({
		url: `/mobile/api/dispatch/selectByWarehouse?warehouse=${json}`,
		method: "get"
	});
};
// 资源调度分析事件下拉
export const getOverIncidentNames = json => {
	return axios({
		url: `/mobile/api/incident/getOverIncidentName?flag=all`,
		method: "get"
	});
};
// 资源调度分析echarts图
export const analyse = json => {
	return axios({
		url: `/mobile/api/incident/analyse?incidentId=${json}`,
		method: "get"
	});
};
// 会商上传图片、文件
export const upload = json => {
	return axios({
		url: `/mobile/api/upload`,
		method: "post",
		data:json
	});
};
// 信息列表
export const messageList = (json,current) => {
	return axios({
		url: `/mobile/api/sms/list?currentPage=${current}&startTime=${json.startTime}&endTime=${json.endTime}&content=${json.content}&pageSize=10`,
		method: "post"
	});
};
// 通信录查询
export const getAll = () => {
	return axios({
		url: `/mobile/note/getAll`,
		method: "get"
	});
};