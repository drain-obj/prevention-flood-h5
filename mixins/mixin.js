export const myMixin = {
	methods:{
		getColor(value){
			if(value === '待审核'){
			  return '#e8555b'
			}else if(value === '待处置'){
			  return '#f5be00 '
			}else if( value === '已处置') {
			  return '#428ee3' 
			}else if( value === '已解除') {
			  return '#81c26b'
			}else if( value === '已关闭') {
			  return '#99a1ad'
			}
		},
		getBgColor(value){
			if(value === '1'){
			  return '#ff5500'
			}else if(value === '2'){
			  return '#00aaff '
			}else if( value === '3') {
			  return '#55aa00' 
			}
		},
		getLevel(level){
			if(level === '1'){
			  return '一级'
			}else if(level === '2'){
			  return '二级'
			}else {
			  return '三级' 
			}
		},
		getLevelColor(level){
			if(level === '1'){
			  return '#e8565a'
			}else if(level === '2'){
			  return '#faa762'
			}else {
			  return '#f5be00' 
			}
		},
	}
}