export default class {

    static transStatus(item) {
        const statusMapping = {
            1: "待处置",
            2: "处置中",
            3: "已处置",
        };
        let key = item.status + "";
        item.statusLable = statusMapping[key];
    }

    static transType(item) {
        const typeMapping = {
            1: "内涝积水",
            2: "工程险情",
            3: "人员转移",
            4: "物资调度",
            5: "力量调度",
        };
        let key = item.type + "";
        item.typeLable = typeMapping[key];
    }

    static transStagnantDepth(item) {
        const depthMapping = {
            1: "30cm以下",
            2: "30cm-50cm",
            3: "50cm以上",
        };
        let key = item.stagDepth + "";
        item.depthLable = depthMapping[key];
    }

    static transStagnantImpact(item) {
        const impactMapping = {
            1: "不影响交通",
            2: "影响交通",
        };
        let key = item.stagAffect + "";
        item.impactLable = impactMapping[key];
    }

    static transStagnantArea(item) {
        const areaMapping = {
            1: "1000m²以下",
            2: "1000m²-3000m²",
            3: "3000m²以上",
        };
        let key = item.stagArea + "";
        item.areaLable = areaMapping[key];
    }

    static transStagnantHandle(item) {
        const mapping = {
            1: "人工助排",
            2: "清理管网",
            3: "改造收水井",
            4:"其他"
        };
        let key = item.handleWay + "";
        item.handleWayLable = mapping[key];
    }
 
    static transDangerHandle(item) {
        const mapping = {
            1: "开闸放水",
            2: "堤防巩固",
            3: "泵车抽水",
            4:"其他"
        };
        let key = item.handleWay + "";
        item.handleWayLable = mapping[key];
    }

    static transPeopleHandle(item) {
        const mapping = {
            1: "危险区域",
            2: "警戒区域",
            3: "转移信号",
            4:"其他"
        };
        let key = item.transferWay + "";
        item.handleWayLable = mapping[key];
    }

    static transDangerType(item) {
        const mapping = {
            1: "满溢",
            2: "渗水",
            3: "渗漏",
            4: "崩塌",
            5: "其他",
        };
        let key = item.engiType + "";
        item.dangerTypeLable = mapping[key];
    }

    static transDangerAera(item) {
        const mapping = {
            1: "1000m²以下",
            2: "1000m²-3000m²",
            3: "3000m²以上",
        };
        let key = item.affectArea + "";
        item.dangerAreaLable = mapping[key];
    }


    static transMaterials(item){
        //materialStoreName: "雨靴:0, 救生圈:55, 编织袋:0, 抽水机:0, 救生绳:0, 冲锋舟:0, 雨衣:0, 救生衣:0, 铲子:66, 橡皮艇:0"
        let res = [];
        let materialsString =  item.materialNameNum;
		if(materialsString){
			let arr = materialsString.split(",");
			for(let i=0;i<arr.length;i++){
			    let kvs = arr[i].split(":");
			    let name = kvs[0];
			    let number = kvs[1];
			    res.push({
			        materialName:name,
			        materialNumber:number
			    })
			}
		}
        
        item.materials = res;
    }

    static transMaterialsHandle(item){
        item.handleWayLable = item.materialStoreName;
    }

    static tranformOperate(item) {
        // const operateMapping = {
        //     0: "派发",
        //     1: "转派",
        // };
        // // status=3 为已处置
        // if (item.status === 3 || item.operate === null) {
        //     item.operateLable = "";
        // } else {
        //     let key = item.operate + "";
        //     let operateLable = operateMapping[key];
        //     item.operateLable = operateLable;
        // }

        // status=3 为已处置
        if (item.status === 3) {
            item.operateLable = "";
        } else if(item.status === 2){
            item.operateLable = "转派"

        }else if(item.status === 1){
            item.operateLable = "派发"
        }
    }

    static tranformDate(item) {
        (function () {
            if (typeof Date.prototype.myFormat !== "function") {
                Date.prototype.myFormat = function (fmt) {
                    var o = {
                        "M+": this.getMonth() + 1, //月份
                        "d+": this.getDate(), //日
                        "h+": this.getHours(), //小时
                        "m+": this.getMinutes(), //分
                        "s+": this.getSeconds(), //秒
                        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                        S: this.getMilliseconds(), //毫秒
                    };
                    if (/(y+)/.test(fmt))
                        fmt = fmt.replace(
                            RegExp.$1,
                            (this.getFullYear() + "").substr(4 - RegExp.$1.length)
                        );
                    for (var k in o)
                        if (new RegExp("(" + k + ")").test(fmt))
                            fmt = fmt.replace(
                                RegExp.$1,
                                RegExp.$1.length == 1
                                    ? o[k]
                                    : ("00" + o[k]).substr(("" + o[k]).length)
                            );
                    return fmt;
                };
            }
        })()

        let milliseconds = item.sendTime;
        milliseconds = Number(milliseconds);
        let dateLable = new Date(milliseconds).myFormat("yyyy/MM/dd hh:mm");
        item.dateLable = dateLable;
    }

}

