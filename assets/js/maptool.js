//import mapJs from '../lib/maps.js'
import {
	Et
} from "./Oak.js";
var map = null;
var routePlanFlag = false;	//是否开启路径规划
var routePlanPtObj = {
	"ROUTEPLAN_BEGIN": null,
	"ROUTEPLAN_END": null
};	//路径规划起终点点位
var driving = null;	//路径规划
var marker;
let iconMap = {
	"MONITOR_RAIN": require("@/assets/images/point/monitor_rain.png"),
	"MONITOR_SW": require("@/assets/images/point/monitor_sw.png"),
	"MONITOR_REO": require("@/assets/images/point/monitor_reo.png"),
	"MONITOR_RIVER": require("@/assets/images/point/monitor_river.png"),
	"MONITOR_PIPE": require("@/assets/images/point/monitor_pipe.png"),
	"MONITOR_WATER": require("@/assets/images/point/monitor_water.png"),
	"MONITOR_RG": require("@/assets/images/point/water_danger.png"),
	"MONITOR_GC": require("@/assets/images/point/engineering_danger.png"),
	"MONITOR_aa": require("@/assets/images/point/icon_river_red.png"),
	"MONITOR_bb": require("@/assets/images/point/icon_water_red.png"),
	"MONITOR_CK":require("@/assets/images/point/monitor_wuzi.png"),
	"MONITOR_SP":require("@/assets/images/point/monitor_shipin.png"),
	"ROUTEPLAN_BEGIN": require("@/assets/images/point/icon_start.png"),
	"ROUTEPLAN_END": require("@/assets/images/point/icon_over.png")
}
export default {


	initMap() {
		map = new AMap.Map('map', {
			resizeEnable: true,
			zoom: 22, //级别
			center: [113.115622, 23.043764], //中心点坐标
			viewMode: '3D' //使用3D视图
		});
		AMap.plugin([
			'AMap.ToolBar'
		], function() {
			// 在图面添加工具条控件，工具条控件集成了缩放、平移、定位等功能按钮在内的组合控件
			map.addControl(new AMap.ToolBar({
				position: "LT",
				liteStyle: true
			}));
		});

		// 触发事件的对象
		map.on('click', function(ev) {
			//当前位置经纬度
			if(routePlanFlag){	//表示当前状态为路径规划，进行地图选点
				Et.Manager.dispatch("routePlanClick", ev.lnglat.lng, ev.lnglat.lat);
			}
		});
	},
	setZoomAndCenter(data) {
		map.setZoomAndCenter(14, [data.lgtd, data.lttd]);
	},
	/**
	 * 请求接口渲染的地图数据
	 */
	randerMarker(locationList) {
		let addMarkerList;
		map.clearMap(); //清除marker
		if(JSON.stringify(locationList) == '{}'){
			return false
		}
		addMarkerList = locationList.features.map(item => {
			let {
				coordinates
			} = item.geometry
			let {
				properties
			} = item
			let lgt = coordinates[0],
				lat = coordinates[1]
			if (!isNaN(Number(lgt)) && !isNaN(Number(lat))) {
				if(properties.isAlarm){
					let images = new AMap.Icon({
						size: new AMap.Size(45, 45),
						image: iconMap[locationList.layerId=="MONITOR_RIVER"?'MONITOR_aa':'MONITOR_bb'],
						imageSize: new AMap.Size(30, 30),
					});
					marker = new AMap.Marker({
						position: [lgt * 1, lat * 1],
						icon: images,
						offset: new AMap.Pixel(-15, -30)
					})
				}else{
					let images = new AMap.Icon({
						size: new AMap.Size(45, 45),
						image: iconMap[locationList.layerId],
						imageSize: new AMap.Size(30, 30),
					});
					marker = new AMap.Marker({
						position: [lgt * 1, lat * 1],
						icon: images,
						offset: new AMap.Pixel(-15, -30)
					})
				}
				let title = '';
				marker.on('click', function(e) {
					Et.Manager.dispatch("pointChange", properties, locationList.layerId)
				});
				marker.id = item.properties.id;
				return marker;
			} else {
				return {}
			}
		})

		map.add(addMarkerList);

		// map.setCenter([locationList[0].geometry.coordinates[0], locationList[0].geometry.coordinates[1]]); //设置地图中心点
		//设置地图显示范围
		var mybounds = new AMap.Bounds([locationList.bbox[0], locationList.bbox[1]], [locationList.bbox[2], locationList
			.bbox[3]
		]);
		map.setBounds(mybounds);
		return addMarkerList;
	},
	// moveCenter(long, lat){
	// 	map.setCenter(long, lat); //设置地图中心点
	// },
	//点击列表某条数据渲染地图数据 不请求接口
	currentMarker(currentData, title) {
		let images;
		map.clearMap(); //清除marker
		images = new AMap.Icon({
			size: new AMap.Size(25, 34),
			image: iconMap[currentData.monitorTypeCode],
			imageSize: new AMap.Size(30, 30),
		});
		marker = new AMap.Marker({
			position: [currentData.lgt * 1, currentData.lat * 1],
			icon: images,
		})
		marker.on('click', function(e) {
			this.setLabel({
				offset: new AMap.Pixel(0, -10), //设置文本标注偏移量
				content: "<div class='info'>" + title + "<i></i></div>", //设置文本标注内容
				direction: 'top' //设置文本标注方位
			});
		})

		map.add(marker);
		map.setCenter([currentData.lgt, currentData.lat]); //设置地图中心点
	},
	/**
	 * 是否启用路径规划
	 */
	applyRoutePlan(applyFlag){
		routePlanFlag = applyFlag;
		if(!routePlanFlag){
			//清除绘制的起终点位置信息
			this.clearRoutePlanPt();
		}
		//清除之前绘制的轨迹
		driving && driving.clear();
	},
	/**
     * 路径规划
	 * drivingType number 规划策略
	 * 			1：最快捷模式  2：最经济模式 3：最短距离模式 4：考虑实时路况
	 * startLngLat array 起点经纬度数据
	 * endLngLat array 终点经纬度数据
	 * showDetailDiv div的id 用于显示详细路线信息
	 * onComplete function 执行完成回调函数
     */
	routePlan(drivingType, startLngLat, endLngLat, showDetailDiv, onComplete){
		var that = this;
		AMap.plugin('AMap.Driving', function() {
			// 驾车路线规划策略
			var drivingPolicy = AMap.DrivingPolicy.LEAST_TIME;
			switch(drivingType){
				case 1:	//最快捷模式
					drivingPolicy = AMap.DrivingPolicy.LEAST_TIME;
					break;
				case 2:	//最经济模式
					drivingPolicy = AMap.DrivingPolicy.LEAST_FEE;
					break;
				case 3:	//最短距离模式
					drivingPolicy = AMap.DrivingPolicy.LEAST_DISTANCE;
					break;
				case 4:	//考虑实时路况
					drivingPolicy = AMap.DrivingPolicy.REAL_TRAFFIC;
					break;
			}
			driving = new AMap.Driving({
				policy: drivingPolicy,
				map: map
			})
			driving.search(startLngLat, endLngLat, function(status, result){
				if(typeof onComplete == 'function'){
					//清除绘制的起终点位置信息
					that.clearRoutePlanPt();
					onComplete(status, result);
				}	
			})
		})
	},
	/**
	 * 绘制路径规划点选位置
	 * @param lnglat array 经纬度
	 * @param imgType string 图标类型	ROUTEPLAN_BEGIN：起点 ROUTEPLAN_END：终点
	 * @param zoomFlag boolean 是否缩放
	 */
	routePlanPt(lnglat, imgType, zoomFlag){
		let routePlanPt = routePlanPtObj[imgType];
		//存在则移除对象
		if(routePlanPt){
			map.remove(routePlanPt);
			routePlanPtObj[imgType] = null;
		}
		var icon = new AMap.Icon({
			size: new AMap.Size(25, 34),
			image: iconMap[imgType],
			imageSize: new AMap.Size(30, 30),
		});
		var marker = new AMap.Marker({
			position: lnglat,
			icon: icon,
			offset: new AMap.Pixel(-15, -30)
		});
		map.add(marker);
		routePlanPtObj[imgType] = marker;
		//缩放定位
		zoomFlag && map.setCenter(lnglat); 
	},
	/**
	 * 移除路径规划起终点位置点位
	 */
	clearRoutePlanPt(){
		Object.keys(routePlanPtObj).forEach(function(key){
			if(routePlanPtObj[key]){
				map.remove(routePlanPtObj[key]);
				routePlanPtObj[key] = null;
			}
		});
	},
	/**
	 * 释放地图资源
	 */
	destroyMap() {
		map && map.destroy();
	}
}
