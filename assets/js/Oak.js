"use strict";
/**
 * 工具类
 */
Object.defineProperty(exports, "__esModule", { value: true });
var Util;
(function (Util) {
    var Map;
    (function (Map_1) {
        var IMapNode = /** @class */ (function () {
            function IMapNode() {
            }
            IMapNode.prototype.node = function (g, k, v) {
                return v;
            };
            /**
             *
             * @param {K} k    key
             * @param {V} v    value
             * @returns {boolean}   false继续查询，true终止查询
             */
            IMapNode.prototype.find = function (g, k, v) {
                return false;
            };
            ;
            return IMapNode;
        }());
        Map_1.IMapNode = IMapNode;
        var HashMap = /** @class */ (function () {
            function HashMap() {
                this._keySet = [];
                this._valSet = [];
                this.clear();
            }
            //通过key获取索引
            HashMap.prototype.getIndexByKey = function (key) {
                var count = this._keySet.length;
                for (var index = 0; index < count; index++) {
                    var k = this._keySet[index];
                    if (k == key) {
                        return index;
                    }
                }
                return -1;
            };
            /**
             * 添加键值
             */
            HashMap.prototype.push = function (key, value) {
                var index = this.getIndexByKey(key);
                if (index != -1) {
                    //已存在：刷新值
                    this._valSet[index] = value;
                }
                else {
                    //不存在：添加值
                    this._keySet.push(key);
                    this._valSet.push(value);
                }
                //let k: string = ObjectUtil.Common.convert(key, "string");
                //this[k] = value;
                return this;
            };
            /**
             * 删除键值
             */
            HashMap.prototype.remove = function (key) {
                var index = this.getIndexByKey(key);
                if (index != -1) {
                    this._keySet.splice(index, 1);
                    this._valSet.splice(index, 1);
                    //let k: string = ObjectUtil.Common.convert(key, "string");
                    //delete this[k];
                    return;
                }
                return;
            };
            /**
             * 是否存在键
             */
            HashMap.prototype.has = function (key) {
                var index = this.getIndexByKey(key);
                return index != -1;
            };
            /**
             * 通过key获取键值value
             * @param key
             */
            HashMap.prototype.get = function (key) {
                var index = this.getIndexByKey(key);
                if (index != -1) {
                    var data = this._valSet[index];
                    return data;
                }
                return null;
            };
            /**
             * 获取数据个数
             */
            HashMap.prototype.size = function () {
                return this._keySet.length;
            };
            /**
             * 遍历列表，回调(data:KeyValue<K, V>)
             */
            HashMap.prototype.forEachKeyValue = function (f) {
                var count = this._keySet.length;
                for (var index = 0; index < count; index++) {
                    var k = this._keySet[index];
                    var v = this._valSet[index];
                    var data = { key: k, value: v };
                    f(data);
                }
            };
            /**
             * 遍历列表，回调(K,V)
             */
            HashMap.prototype.forEach = function (f) {
                var count = this._keySet.length;
                for (var index = 0; index < count; index++) {
                    var k = this._keySet[index];
                    var v = this._valSet[index];
                    f(k, v);
                }
            };
            /**
             * 清空全部
             */
            HashMap.prototype.clear = function () {
                if (!ObjectUtil.Common.isEmpty(this._keySet)) {
                    /*this._keySet.forEach(key => {
                      let _k: string = ObjectUtil.Common.convert(key, "string");
                      delete this[_k];
                    });*/
                }
                this._keySet = [];
                this._valSet = [];
            };
            Object.defineProperty(HashMap.prototype, "keySet", {
                get: function () {
                    return this._keySet;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HashMap.prototype, "valSet", {
                get: function () {
                    return this._valSet;
                },
                enumerable: true,
                configurable: true
            });
            HashMap.prototype.keys = function () {
                return this.keySet;
            };
            HashMap.prototype.values = function () {
                return this.valSet;
            };
            HashMap.prototype.firstKey = function () {
                var len = this._keySet.length;
                return len > 0 ? this._keySet[0] : null;
            };
            HashMap.prototype.firstVal = function () {
                var len = this._valSet.length;
                return len > 0 ? this._valSet[0] : null;
            };
            HashMap.prototype.lastKey = function () {
                var len = this._keySet.length;
                return len > 0 ? this._keySet[len - 1] : null;
            };
            HashMap.prototype.lastVal = function () {
                var len = this._valSet.length;
                return len > 0 ? this._valSet[len - 1] : null;
            };
            return HashMap;
        }());
        Map_1.HashMap = HashMap;
        /*export class MapPath {

          private resultType: string;
          private result: Array<any>;
          private $: any;


          constructor(obj: any, expr: string, arg: any) {
            this.$ = obj;
            this.resultType = arg && arg.resultType || "VALUE";
            this.result = [];
          }

          private asPath(path: string): string {
            let array: string[] = path.split(";");
            let p: string = "$";
            for (let i: number = 1, len: number = array.length; i < len; i++)
              p += /^[0-9*]+$/.test(array[i]) ? "[" + array[i] + "]" : "['" + array[i] + "']";
            return p;
          }

          private store(p: string, v: string): boolean {
            if (p) {
              this.result[this.result.length] = this.resultType == "PATH" ? this.asPath(p) : v;
            }
            return !!p;
          }

          private normalize(expr: string): any {
            let subX: Array<any> = [];
            return expr.replace(/[\['](\??\(.*?\))[\]']/g, ($0, $1) => {
              return "[#" + (subX.push($1) - 1) + "]";
            }).replace(/'?\.'?|\['?/g, ";").replace(/;;;|;;/g, ";..;").replace(/;$|'?]|'$/g, "").replace(/#([0-9]+)/g, function ($0, $1) {
              return subX[$1];
            });
          }


          private walk(loc, expr: string, val: any, path: string, f: any): void {
            if (val instanceof Array) {
              for (var i = 0, n = val.length; i < n; i++) {
                if (i in val) {
                  f(i, loc, expr, val, path);
                }
              }
            } else if (typeof val === "object") {
              for (var m in val) {
                if (val.hasOwnProperty(m)) {
                  f(m, loc, expr, val, path);
                }
              }
            }
          }

          private trace(expr: string, val: any, path: string) {
            if (expr) {
              let x: Array<any> = expr.split(";");
              let loc: string = x.shift();
              let _x: string = x.join(";");
              if (val && val.hasOwnProperty(loc)) {
                this.trace(_x, val[loc], path + ";" + loc);
              } else if (loc === "*") {
                this.walk(loc, _x, val, path, (m, l, x, v, p) => {
                  this.trace(m + ";" + x, v, p);
                });
              } else if (loc === "..") {
                this.trace(_x, val, path);
                this.walk(loc, _x, val, path, (m, l, x, v, p) => {
                  typeof v[m] === "object" && this.trace("..;" + x, v[m], p + ";" + m);
                });
              } else if (/,/.test(loc)) { // [name1,name2,...]
                for (var s = loc.split(/'?,'?/), i = 0, n = s.length; i < n; i++) {
                  this.trace(s[i] + ";" + x, val, path);
                }
              } else if (/^\(.*?\)$/.test(loc)) {// [(expr)]
                //this.trace(this.eval(loc, val, path.substr(path.lastIndexOf(';') + 1)) + ';' + x, val, path);
                path = path.substr(path.lastIndexOf(";") + 1) + ";" + x;
                this.trace(this.eval(loc, val), val, path);
              } else if (/^\?\(.*?\)$/.test(loc)) {// [?(expr)]
                this.walk(loc, _x, val, path, (m, l, x, v, p) => {
                  let _l = l.replace(/^\?\((.*?)\)$/, "$1");
                  //if (this.eval(_l, v[m], m)) {
                  if (this.eval(_l, v[m])) {
                    this.trace(m + ";" + x, v, p);
                  }
                });
              } else if (/^(-?[0-9]*):(-?[0-9]*):?([0-9]*)$/.test(loc)) {// [start:end:step]  phyton slice syntax
                this.slice(loc, _x, val, path);
              }
            } else {
              this.store(path, val);
            }
          }


          private slice(loc: any, expr: string, val, path: string) {
            if (val instanceof Array) {
              let len: number = val.length;
              let start: number = 0;
              let end: number = len;
              let step: number = 1;
              loc.replace(/^(-?[0-9]*):(-?[0-9]*):?(-?[0-9]*)$/g, function ($0, $1, $2, $3) {
                start = parseInt($1 || start);
                end = parseInt($2 || end);
                step = parseInt($3 || step);
              });
              start = start < 0 ? Math.max(0, start + len) : Math.min(len, start);
              end = end < 0 ? Math.max(0, end + len) : Math.min(len, end);
              for (var i = start; i < end; i += step)
                this.trace(i + ";" + expr, val, path);
            }
          }

          private eval(x: string, _v: any): any {
            try {
              return this.$ && _v && eval(x.replace(/@/g, "_v"));
            } catch (e) {
              throw new SyntaxError("jsonPath: " + e.message + ": " + x.replace(/@/g, "_v").replace(/\^/g, "_a"));
            }
          }
        }*/
    })(Map = Util.Map || (Util.Map = {}));
    var HashMap = Util.Map.HashMap;
    var CacheManager = /** @class */ (function () {
        function CacheManager() {
            this.cache = new HashMap();
        }
        CacheManager.getInstance = function () {
            if (!CacheManager.cacheManager) {
                CacheManager.cacheManager = new CacheManager();
            }
            return CacheManager.cacheManager;
        };
        CacheManager.prototype.setData = function (key, val) {
            this.cache.push(key, val);
            return this;
        };
        CacheManager.prototype.getData = function (key) {
            return this.cache.get(key);
        };
        CacheManager.prototype.hasData = function (key) {
            return this.cache.has(key);
        };
        /**
         * 类似 Java String...
         * @param {string} keys
         * @returns {any[]}
         */
        CacheManager.prototype.getItems = function () {
            var keys = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                keys[_i] = arguments[_i];
            }
            var isEmpty = ObjectUtil.Common.isEmpty(keys);
            if (isEmpty) {
                var _keys = this.cache.keys();
                keys = [];
                for (var _a = 0, _keys_1 = _keys; _a < _keys_1.length; _a++) {
                    var key = _keys_1[_a];
                    keys.push(key);
                }
            }
            var items = new Array;
            var item;
            for (var i = 0; i < keys.length; i++) {
                item = this.getData(keys[i]);
                items.push(item);
            }
            return items;
        };
        return CacheManager;
    }());
    Util.CacheManager = CacheManager;
})(Util = exports.Util || (exports.Util = {}));
/**
 * 帮住类
 */
var ObjectUtil;
(function (ObjectUtil) {
    var HashMap = Util.Map.HashMap;
    var Common = /** @class */ (function () {
        function Common() {
        }
        Common.stringifyFunction = function (obj) {
            if (Common.isString(obj))
                return obj;
            var newobj = JSON.parse(JSON.stringify(obj));
            for (var key in obj) {
                if (obj[key] instanceof Function) {
                    newobj[key] = obj[key].toString().replace(/[\n\t]/g, "");
                    continue;
                }
                if (obj[key] instanceof Object) {
                    newobj[key] = Common.stringifyFunction(obj[key]);
                }
            }
            return newobj;
        };
        Common.stringToFunction = function (obj) {
            var regex = /^((function\s)|.)([a-zA-Z_][a-zA-Z0-9_]*)\(.*\)\s\{.*\}/; //匹配函数字符串
            for (var key in obj) {
                if (obj[key] instanceof Object) {
                    Common.stringToFunction(obj[key]);
                }
                else {
                    if (regex.test(obj[key])) { // 是一个函数
                        try {
                            var params = obj[key].substring(obj[key].indexOf('(') + 1, obj[key].indexOf(')'));
                            var operation = obj[key].substring(obj[key].indexOf("{") + 1, obj[key].length - 1);
                            obj[key] = new Function(params, operation);
                        }
                        catch (e) {
                            console.log(e);
                        }
                    }
                }
            }
            return obj;
        };
        /**
         * 对象转字符串，并暴力方法
         * @param obj
         * @returns {string}
         */
        Common.obj2String = function (obj) {
            var _object = Common.stringifyFunction(obj);
            return JSON.stringify(_object);
        };
        /**
         *
         * @param string
         * @returns {any}
         */
        Common.string2Obj = function (string) {
            var obj = JSON.parse(string);
            return this.stringToFunction(obj);
        };
        Common.hashCode = function (o) {
            var _string = Common.obj2String(o);
            var hash = 0;
            var chr = null;
            if (_string.length === 0)
                return hash;
            for (var i = 0, len = _string.length; i < len; i++) {
                chr = _string.charCodeAt(i);
                hash = ((hash << 5) - hash) + chr;
                hash |= 0;
            }
            return hash;
        };
        Common.uuid36 = function () {
            var temp = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx";
            var uuid = temp.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == "x" ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
            return uuid;
        };
        Common.uuid32 = function () {
            var uuid36 = this.uuid36();
            var uuid32 = ObjectUtil.Strings.replaceAll(uuid36, "-", "");
            return uuid32.toUpperCase();
        };
        Common.convert = function (o, clazz) {
            if (this.isEmpty(clazz))
                return o;
            if (this.isEmpty(o))
                return o;
            if (this.isString(clazz))
                clazz = clazz.toLowerCase();
            if (clazz == "number") {
                if (isNaN(o))
                    return null;
                return Number(o);
            }
            else if (clazz == "string") {
                if (this.isDate(o)) {
                    return Times.formatDate(o, "yyyy-MM-dd hh:ss:mm");
                }
                else {
                    if (this.isString(o))
                        return o;
                    else
                        return JSON.stringify(o);
                }
            }
            else if (clazz == "boolean") {
                if (o.toString().toLowerCase() == "false")
                    return false;
                if (o.toString().toLowerCase() == "true")
                    return true;
                return null;
            }
            else if (clazz == "date") {
                var date = new Date(o);
                if (date.toString() == 'Invalid Date')
                    return null;
                return date;
            }
            else { //转换成‘复杂’对象
                var jsonObj = this.toJsonObj(o);
                var t = this._assign(jsonObj, clazz);
                return t;
            }
        };
        Common._assign = function (source, target) {
            var _output = null;
            if (source == undefined && source == null
                || target == null || target == undefined)
                return _output;
            if (this.isArray(source)) {
                _output = new Array();
                for (var i = 0; i < source.length; i++) {
                    var item = Object(target);
                    var _source = source[i];
                    if (_source == null || _source == undefined)
                        continue;
                    for (var _key_1 in target) {
                        var key = _key_1;
                        if (Strings.startWith(_key_1, "_"))
                            key = Strings.replaceFirst(_key_1, "_", "");
                        if (_source.hasOwnProperty(_key_1)) {
                            item[_key_1] = _source[_key_1];
                        }
                        else if (_source.hasOwnProperty(key)) {
                            item[key] = _source[key];
                        }
                    }
                    _output.push(item);
                }
            }
            else if (this.isMap(source)) {
                _output = Object(target);
                for (var _key in target) {
                    var key = _key;
                    if (Strings.startWith(_key, "_"))
                        key = Strings.replaceFirst(_key, "_", "");
                    if (source.hasOwnProperty(_key)) {
                        _output[_key] = source[_key];
                    }
                    else if (source.hasOwnProperty(key)) {
                        _output[key] = source[key];
                    }
                }
            }
            return _output;
        };
        /**
         * 判断对象是否字面相等
         * @param o1
         * @param o2
         * @returns {boolean}
         */
        Common.faceEqual = function (o1, o2) {
            if (!o1 || !o2)
                return false;
            var s1 = JSON.stringify(o1);
            var s2 = JSON.stringify(o2);
            return s1 == s2;
        };
        Common.faceEqualIgnore = function (o1, o2) {
            if (this.isEmpty(o1) || this.isEmpty(o2))
                return false;
            var s1 = JSON.stringify(o1).toLowerCase();
            var s2 = JSON.stringify(o2).toLowerCase();
            return s1 == s2;
        };
        /**
         * 对象深克隆
         * @param o
         * @returns {any}
         */
        Common.clone = function (o) {
            if (!o)
                return null;
            return JSON.parse(JSON.stringify(o));
        };
        Common.toJsonObj = function (o) {
            if (this.isString(o)) {
                return JSON.parse(o);
            }
            return o;
        };
        /**
         *
         * @param o
         * @returns {boolean}
         */
        Common.isString = function (o) {
            if (!o)
                return false;
            return (typeof o == "string") && o.constructor == String;
        };
        Common.isNumber = function (o) {
            if (o === 0)
                return true;
            if (!o)
                return false;
            return (typeof o == "number") && o.constructor == Number;
        };
        Common.isDate = function (o) {
            if (!o)
                return false;
            if (o.toString() == 'Invalid Date')
                return false;
            return (typeof o == "object") && o.constructor == Date;
        };
        Common.isFun = function (o) {
            if (!o)
                return false;
            return (typeof o == "function") && o.constructor == Function;
        };
        Common.isMap = function (o) {
            try {
                if (this.isString(o))
                    return false;
                var str = JSON.stringify(o);
                if (Strings.startWith(str, "{")
                    && Strings.endWith(str, "}"))
                    return true;
                return false;
            }
            catch (e) {
                return true;
            }
        };
        Common.isArray = function (o) {
            if (!o)
                return false;
            if (typeof Array.isArray === "function") {
                return Array.isArray(o);
            }
            else {
                return Object.prototype.toString.call(o) === "[object Array]";
            }
        };
        Common.isEmpty = function (obj) {
            if (obj === "0" || obj === 0 || obj === "false" || obj === false)
                return false;
            if (!obj)
                return true;
            if (this.isArray(obj)) {
                return obj.length == 0;
            }
            if (this.isString(obj)) {
                return obj.length == 0;
            }
            if (this.isMap(obj)) {
                var keys = Object.keys(obj);
                if (!keys || keys.length == 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return false;
        };
        Common.compare = function (o1, o2) {
            if (this.isEmpty(o1))
                return 1;
            if (this.isEmpty(o2))
                return 2;
            if (isNaN(o1) || isNaN(o2)) {
                var s1 = void 0;
                var s2 = void 0;
                if (this.isDate(o1)) {
                    s1 = Common.convert(o1, "date");
                }
                else {
                    s1 = Common.convert(o1, "string");
                }
                if (this.isDate(o2)) {
                    s2 = Common.convert(o2, "date");
                }
                else {
                    s2 = Common.convert(o2, "string");
                }
                console.log("字符串比较:", o1 + "|" + o2);
                return s1.localeCompare(s2);
            }
            else {
                var n1 = this.convert(o1, "number");
                var n2 = this.convert(o2, "number");
                console.log("数字比较:", o1 + "|" + o2);
                if (n1 < n2)
                    return 1;
                if (n1 > n2)
                    return 2;
                return 0;
            }
        };
        Common.toFix = function (o, accaury) {
            if (this.isEmpty(o))
                return null;
            var num = this.convert(o, "number");
            accaury = this.convert(accaury, "number");
            if (this.isEmpty(accaury))
                accaury = 0;
            num = this.convert(num.toFixed(accaury), "number");
            return num;
        };
        //-----------------------------------------------对象遍历-------------------------------------------------------
        Common.getIndex = function (o) {
            var keyWords = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                keyWords[_i - 1] = arguments[_i];
            }
            if (this.isEmpty(o) || this.isEmpty(keyWords))
                return [];
            var globalIndex = new HashMap();
            var mapNoe = {
                find: function (g, k, v) {
                    var has = Collect.hasAllKeyWords((_a = {}, _a[k] = v, _a), keyWords);
                    if (has)
                        globalIndex.push(g, v);
                    return false;
                    var _a;
                },
                node: function (g, k, v) {
                    return v;
                }
            };
            this._findObj(o, mapNoe);
            return globalIndex.keys();
        };
        Common.find = function (o, path) {
            var globalIndex = new HashMap();
            var partIndex = new HashMap();
            var mapNoe = {
                find: function (g, k, v) {
                    if (g === path) {
                        globalIndex.push(g, v);
                        partIndex.push(k, v);
                        return true;
                    }
                    return false;
                },
                node: function (g, k, v) {
                    return v;
                }
            };
            this._findObj(o, mapNoe);
            return globalIndex.firstVal();
        };
        Common.match = function (o, key) {
            var globalIndex = new HashMap();
            var partIndex = new HashMap();
            var mapNoe = {
                find: function (g, k, v) {
                    console.log(g, v);
                    globalIndex.push(g, v);
                    partIndex.push(k, v);
                    return false;
                },
                node: function (g, k, v) {
                    return v;
                }
            };
            this._findObj(o, mapNoe);
            return null;
        };
        Common._findObj = function (o, mapNode) {
            if (this.isEmpty(o) || this.isEmpty(mapNode))
                return;
            if (this.isMap(o)) {
                this._findMap(o, null, mapNode);
            }
            else if (this.isArray(o)) {
                this._findArray(o, null, mapNode);
            }
            /*else {
                mapNode.find(null, null, o);
            }*/
        };
        Common._findArray = function (array, g, mapNode) {
            var item;
            var global;
            for (var i = 0, len = array.length; i < len; i++) {
                global = g == null ? "[" + i + "]" : g + ".[" + i + "]";
                item = array[i];
                if (this.isMap(item)) {
                    this._findMap(item, global, mapNode);
                }
                else if (this.isArray(item)) {
                    this._findArray(item, global, mapNode);
                }
                else {
                    var isFinish = mapNode.find(global, "[" + i + "]", item);
                    if (isFinish)
                        return;
                }
            }
        };
        Common._findMap = function (o, g, mapNode) {
            var key;
            var item;
            var isFinish;
            var keys = Object.keys(o);
            var global;
            for (var i = 0, len = keys.length; i < len; i++) {
                key = keys[i];
                global = g == null ? key : g + "." + key;
                item = o[key];
                isFinish = mapNode.find(global, key, item);
                if (isFinish)
                    return;
                if (this.isMap(item)) {
                    this._findMap(item, global, mapNode);
                }
                else if (this.isArray(item)) {
                    this._findArray(item, global, mapNode);
                }
            }
        };
        /**
         * 获取前端显示文字
         * @param obj
         * @param {string} field
         */
        Common.getDisplay = function (o, field) {
            var val = this.getByPath(o, field);
            if (ObjectUtil.Common.isEmpty(val))
                return "--";
            else
                return val;
        };
		Common.safeDisplay = function (o) {
			if (ObjectUtil.Common.isEmpty(o))
				return "--";
			if (!isNaN(o)) {
				var num = ObjectUtil.Common.toFix(o, 3);
				return num;
			}
			else {
				return o;
			}
		};
        /**
         *
         * @param o
         * @param {string} path
         * @returns {any}
         */
        Common.getByPath = function (o, path) {
            var target = null;
            try {
                if (Strings.startWith(path, "$."))
                    path = Strings.replaceFirst(path, "$.", "");
                var code = "target =o." + path;
                eval(code);
            }
            catch (e) {
                //console.log(e)
            }
            return target;
        };
        /**
         *
         * @param o
         * @param {string} path
         * @param v
         * @returns {any}
         */
        Common.setByPath = function (o, path, v) {
            if (Strings.startWith(path, "$."))
                path = Strings.replaceFirst(path, "$.", "");
            var code = "o." + path + "= v";
            eval(code);
            return o;
        };
        return Common;
    }());
    ObjectUtil.Common = Common;
    var Times = /** @class */ (function () {
        function Times() {
        }
        /**
         * 获取当前时间
         * @returns {Date}
         */
        Times.getNowDate = function () {
            return new Date();
        };
        Times.getNowStr = function () {
            var now = this.getNowDate();
            return this.formatDate(now, "yyyy-MM-dd hh:mm:ss");
        };
        /**
         * 格式化日期
         * @param {Date} date
         * @param {string} format  yyyy-MM-dd hh:mm:ss
         * @returns {string}
         */
        Times.formatDate = function (date, format) {
            var _date = {
                "M+": date.getMonth() + 1,
                "d+": date.getDate(),
                "h+": date.getHours(),
                "m+": date.getMinutes(),
                "s+": date.getSeconds(),
                "q+": Math.floor((date.getMonth() + 3) / 3),
                "S": date.getMilliseconds()
            };
            if (/(y+)/.test(format)) {
                format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
            }
            for (var k in _date) {
                var pattern = "(" + k + ")";
                if (new RegExp(pattern).test(format)) {
                    format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? _date[k] : ("00" + _date[k]).substr(("" + _date[k]).length));
                }
            }
            return format;
        };
        Times.getTimeBetween = function () {
        };
        Times.nowSubPlus = function (count) {
            var cur = new Date();
            var timeStamp = cur.getTime() + count * 1000 * 60 * 60 * 24;
            return new Date(timeStamp);
        };
        return Times;
    }());
    ObjectUtil.Times = Times;
    var Json = /** @class */ (function () {
        function Json() {
        }
        Json.isJson = function (arg) {
            try {
                JSON.parse(arg);
                return true;
            }
            catch (e) {
                return false;
            }
        };
        return Json;
    }());
    ObjectUtil.Json = Json;
    var Reflect = /** @class */ (function () {
        function Reflect() {
        }
        return Reflect;
    }());
    ObjectUtil.Reflect = Reflect;
    var Strings = /** @class */ (function () {
        function Strings() {
        }
        /**
         *
         * @param {string} s
         * @param {number} count
         */
        Strings.subUnNullStr = function (s, count) {
            var _s = "";
            var c;
            for (var i = 0, len = s.length; i < len; i++) {
                c = s.charAt(i);
                if (c == " " || c == "\\n")
                    continue;
                _s += c;
                if (_s.length == count)
                    break;
            }
            return _s;
        };
        /**
         * 字符串转义
         * @param {string} s
         * @returns {string}
         */
        Strings.escapeRegExp = function (s) {
            var reRegExpChar = /[\\"'()[\]{}|]/g;
            var reHasRegExpChar = RegExp(reRegExpChar.source);
            var res = (s && reHasRegExpChar.test(s)) ? s.replace(reRegExpChar, '\\$&') : (s || '');
            return res;
        };
        Strings.contains = function (source) {
            var target = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                target[_i - 1] = arguments[_i];
            }
            if (Common.isEmpty(source) || Common.isEmpty(target))
                return false;
            var _target;
            for (var i = 0, len = target.length; i < len; i++) {
                _target = target[i];
                if (Common.isEmpty(_target))
                    return false;
                if (source.indexOf(_target) == -1)
                    return false;
            }
            return true;
        };
        Strings.contain = function (source, target) {
            if (Common.isEmpty(source) || Common.isEmpty(target))
                return false;
            return source.indexOf(target) > -1;
        };
        Strings.replaceFirst = function (source, old, target) {
            if (Common.isEmpty(source) || Common.isEmpty(old) || Common.isEmpty(target))
                return source;
            var _old = Common.convert(old, "string");
            _old = this.escapeRegExp(_old);
            var _target = Common.convert(target, "string");
            source = source.replace(_old, _target);
            return source;
        };
        Strings.replaceAll = function (source, old, target) {
            if (Common.isEmpty(source) || Common.isEmpty(old) || (target != "" && Common.isEmpty(target)))
                return source;
            var _old = Common.convert(old, "string");
            _old = this.escapeRegExp(_old);
            var _target = Common.convert(target, "string");
            source = source.replace(new RegExp(_old, "g"), _target);
            return source;
        };
        Strings.startWith = function (source, flag) {
            if (Common.isEmpty(source) || Common.isEmpty(flag))
                return false;
            flag = this.escapeRegExp(flag);
            var reg = new RegExp("^" + flag);
            return reg.test(source);
        };
        Strings.endWith = function (source, flag) {
            if (Common.isEmpty(source) || Common.isEmpty(flag))
                return false;
            flag = this.escapeRegExp(flag);
            var reg = new RegExp(flag + "$");
            return reg.test(source);
        };
        /**
         * 两个字符串的相似程度，并返回相似度百分比
         * @param {string} s
         * @param {string} t
         * @returns {number}
         */
        Strings.similar = function (source, target) {
            var len = source.length > target.length ? source.length : target.length;
            var d = this._similar(source, target);
            return 1 - d / len;
        };
        /**
         * 字符串相似度比较
         * @param {string} s
         * @param {string} t
         * @returns {any}
         */
        Strings._similar = function (source, target) {
            var n = source.length;
            var m = target.length;
            if (n == 0)
                return m;
            if (m == 0)
                return n;
            var sourceChart;
            var targetChart;
            var cost;
            var step = new Array();
            for (var i = 0; i <= n; i++) {
                step[i] = [];
                step[i][0] = i;
            }
            for (var j = 0; j <= m; j++) {
                step[0][j] = j;
            }
            for (var i = 1; i <= n; i++) {
                sourceChart = source.charAt(i - 1);
                for (var j = 1; j <= m; j++) {
                    targetChart = target.charAt(j - 1);
                    if (sourceChart == targetChart) {
                        cost = 0;
                    }
                    else {
                        cost = 1;
                    }
                    step[i][j] = this._minimum(step[i - 1][j] + 1, step[i][j - 1] + 1, step[i - 1][j - 1] + cost);
                }
            }
            return step[n][m];
        };
        /**
         *
         * @param {number} a
         * @param {number} b
         * @param {number} c
         * @returns {number}
         * @constructor
         */
        Strings._minimum = function (a, b, c) {
            return a < b ? (a < c ? a : c) : (b < c ? b : c);
        };
        /**
         * 比较多个字符串最长相似部分
         * @param {Array<string>} s1
         * @returns {string}
         */
        Strings.maxLongString = function (s) {
            if (Common.isEmpty(s))
                return null;
            var comCache = [];
            for (var i = 0, lenI = s.length; i < lenI; i++) {
                var s1 = s[i];
                for (var j = i + 1, lenJ = s.length; j < lenJ; j++) {
                    var s2 = s[j];
                    var temp = this.maxLong2String(s1, s2);
                    if (Collect.contains(comCache, temp)) {
                        continue;
                    }
                    comCache.push(temp);
                }
            }
            var longString = null;
            for (var i = 0, lenI = comCache.length; i < lenI; i++) {
                var allContains = true;
                var tempString = comCache[i];
                for (var j = 0, lenJ = s.length; j < lenJ; j++) {
                    if (!Strings.contains(s[j], tempString))
                        allContains = false;
                }
                if (!allContains)
                    continue;
                if (longString == null || longString.length < tempString.length) {
                    longString = tempString;
                }
            }
            return longString;
        };
        /**
         * 比较2个字符串最长相似部分
         * @param {string} s1
         * @param {string} s2
         * @returns {string}
         */
        Strings.maxLong2String = function (s1, s2) {
            if (Common.isEmpty(s1 || Common.isEmpty(s2)))
                return null;
            var len1 = s1.length;
            var len2 = s2.length;
            var a = new Array(len1);
            var maxLen = 0;
            var maxPos = 0;
            for (var i = 0; i < len1; i++) { //行
                for (var j = len2 - 1; j >= 0; j--) { //列
                    if (s1.charAt(j) == s2.charAt(i)) {
                        if (i === 0 || j === 0) {
                            a[j] = 1;
                        }
                        else {
                            a[j] = a[j - 1] + 1;
                        }
                    }
                    else {
                        a[j] = 0;
                    }
                    if (a[j] > maxLen) {
                        maxLen = a[j];
                        maxPos = j;
                    }
                }
            }
            return s1.substr(maxPos - maxLen + 1, maxLen);
        };
        /**
         * 比较两个字符串 起始位置最大相同长度
         * @param {string} s1
         * @param {string} s2
         * @returns {string}
         */
        Strings.maxStart2String = function (s1, s2) {
            if (Common.isEmpty(s1 || Common.isEmpty(s2)))
                return null;
            var long;
            var short;
            if (s1.length > s2.length) {
                long = s1;
                short = s2;
            }
            else {
                long = s2;
                short = s1;
            }
            var max = 0;
            for (var i = 0, len = short.length; i < len; i++) {
                if (short.charAt(i) == long.charAt(i))
                    max++;
                else
                    break;
            }
            return short.substr(0, max);
        };
        /**
         * 按照字符串 共性 分组，组与组之间   各组个数之和 = s的长度（非黑即白）
         * @param {Array<string>} s
         * @returns {string[]}
         */
        Strings.maxLongGroup = function (s) {
            var res = new Array();
            if (Common.isEmpty(s))
                return res;
            /**
             * 获取与任何都没有公共部分的
             */
            var diffCache = new HashMap();
            for (var i = 0, lenI = s.length; i < lenI; i++) {
                var s1 = s[i];
                var hasCommon = false;
                for (var j = 0, lenJ = s.length; j < lenJ; j++) {
                    if (i == j)
                        continue;
                    var s2 = s[j];
                    var temp = this.maxLong2String(s1, s2);
                    if (!Common.isEmpty(temp)) {
                        hasCommon = true;
                        break;
                    }
                }
                if (!hasCommon)
                    diffCache.push(s1, s1);
            }
            //获取公共部分
            var comCache = [];
            for (var i = 0, lenI = s.length; i < lenI; i++) {
                var s1 = s[i];
                for (var j = i + 1, lenJ = s.length; j < lenJ; j++) {
                    var s2 = s[j];
                    var temp = this.maxLong2String(s1, s2);
                    if (Common.isEmpty(temp) || Collect.contains(comCache, temp))
                        continue;
                    comCache.push(temp);
                }
            }
            comCache = comCache.concat(diffCache.keys());
            var groupCache = new HashMap();
            for (var i = 0, lenI = comCache.length; i < lenI; i++) {
                var groupKey_1 = comCache[i];
                for (var j = 0, lenJ = s.length; j < lenJ; j++) {
                    if (!Strings.contains(s[j], groupKey_1))
                        continue;
                    var group = groupCache.get(groupKey_1);
                    group = group == null ? [] : group;
                    group.push(s[j]);
                    groupCache.push(groupKey_1, group);
                }
            }
            var groupKey = groupCache.keys();
            //得到排列组合
            var permutation = Collect.permutation(groupKey);
            if (Common.isEmpty(permutation))
                return res;
            for (var i = 0, lenI = permutation.length; i < lenI; i++) {
                var array = permutation[i];
                if (Common.isEmpty(array))
                    continue;
                var newArray = [];
                for (var j = 0, lenJ = array.length; j < lenJ; j++) {
                    var temp = groupCache.get(array[j]);
                    newArray = newArray.concat(temp);
                }
                if (newArray.length != s.length)
                    continue;
                if (Collect.diff(newArray, s).length != 0)
                    continue;
                res.push(array);
            }
            return res;
        };
        /**
         *
         * @param {Array<string>} s
         * @returns {Array<any>}
         */
        Strings.maxStartGroup = function (s) {
            var res = new Array();
            if (Common.isEmpty(s))
                return res;
            /**
             * 获取与任何都没有公共部分的
             */
            var diffCache = new HashMap();
            for (var i = 0, lenI = s.length; i < lenI; i++) {
                var s1 = s[i];
                var hasCommon = false;
                for (var j = 0, lenJ = s.length; j < lenJ; j++) {
                    if (i == j)
                        continue;
                    var s2 = s[j];
                    var temp = this.maxStart2String(s1, s2);
                    if (!Common.isEmpty(temp)) {
                        hasCommon = true;
                        break;
                    }
                }
                if (!hasCommon)
                    diffCache.push(s1, s1);
            }
            //获取公共部分
            var comCache = [];
            for (var i = 0, lenI = s.length; i < lenI; i++) {
                var s1 = s[i];
                for (var j = i + 1, lenJ = s.length; j < lenJ; j++) {
                    var s2 = s[j];
                    var temp = this.maxStart2String(s1, s2);
                    if (Common.isEmpty(temp))
                        continue;
                    var hasExe = false;
                    for (var k = 0, lenK = comCache.length; k < lenK; k++) {
                        var item = comCache[k];
                        if (item.length > temp.length) {
                            if (Strings.startWith(item, temp)) {
                                comCache.splice(k, 1, temp);
                                hasExe = true;
                                break;
                            }
                        }
                        else {
                            if (Strings.startWith(temp, item)) {
                                hasExe = true;
                                break;
                            }
                        }
                    }
                    if (!hasExe)
                        comCache.push(temp);
                }
            }
            comCache = comCache.concat(diffCache.keys());
            //startWith
            var startWith = [];
            for (var i = 0, lenI = comCache.length; i < lenI; i++) {
                var temp = comCache[i];
                var has = false;
                for (var j = 0, lenJ = s.length; j < lenJ; j++) {
                    var _s1 = s[j];
                    if (Strings.startWith(_s1, temp)) {
                        has = true;
                        break;
                    }
                }
                if (has)
                    startWith.push(temp);
            }
            comCache = startWith;
            var groupCache = new HashMap();
            for (var i = 0, lenI = comCache.length; i < lenI; i++) {
                var groupKey = comCache[i];
                for (var j = 0, lenJ = s.length; j < lenJ; j++) {
                    if (!Strings.contains(s[j], groupKey))
                        continue;
                    var group = groupCache.get(groupKey);
                    group = group == null ? [] : group;
                    group.push(s[j]);
                    groupCache.push(groupKey, group);
                }
            }
            var array = groupCache.keys();
            //得到排列组合
            if (Common.isEmpty(array))
                return res;
            var newArray = [];
            for (var j = 0, lenJ = array.length; j < lenJ; j++) {
                var temp = groupCache.get(array[j]);
                newArray = newArray.concat(temp);
            }
            if (newArray.length != s.length)
                return res;
            if (Collect.diff(newArray, s).length != 0)
                return res;
            res = res.concat(array);
            return res;
        };
        return Strings;
    }());
    ObjectUtil.Strings = Strings;
    var Collect = /** @class */ (function () {
        function Collect() {
        }
        /**
         * 对象排列组合  参考 概率排列组合
         * @param {string[]} a
         * @returns {any}
         */
        Collect.permutation = function (array) {
            var arraies = this.getGroup(array);
            return arraies;
        };
        Collect.getGroup = function (data, index, group) {
            if (index === void 0) { index = 0; }
            if (group === void 0) { group = []; }
            var need_apply = new Array();
            need_apply.push([data[index]]);
            for (var i = 0; i < group.length; i++) {
                var array = new Array();
                array = array.concat(group[i]);
                array.push(data[index]);
                need_apply.push(array);
            }
            group.push.apply(group, need_apply);
            if (index + 1 >= data.length)
                return group;
            else
                return this.getGroup(data, index + 1, group);
        };
        /**
         * 求数组不相同部分
         * @returns {Array<any>}
         */
        Collect.diff = function (one, tow) {
            var a = one;
            var b = tow;
            var diff = [];
            var tmp = a.concat(b);
            var cache = {};
            for (var i = 0; i < tmp.length; i++) {
                (tmp[i] in cache) ? cache[tmp[i]]++ : cache[tmp[i]] = 1;
            }
            for (var x in cache) {
                if (cache[x] == 1) {
                    diff.push(x);
                }
            }
            return diff;
        };
        Collect.contains = function (array, val) {
            if (Common.isEmpty(array) || Common.isEmpty(val))
                return false;
            var str = ',' + array.join(",") + ",";
            return str.indexOf("," + val + ",") != -1;
        };
        Collect._eachStartWithIndex = function (array, falg) {
            var eachIndex = -1;
            if (Common.isEmpty(array) || Common.isEmpty(falg))
                return eachIndex;
            var item;
            for (var i = 0, len = array.length; i < len; i++) {
                item = array[i];
                if (Strings.startWith(item, falg) || Strings.startWith(falg, item)) {
                    eachIndex = i;
                    break;
                }
            }
            return eachIndex;
        };
        Collect.sort = function (array) {
            if (!Common.isArray(array))
                return null;
            array.sort(function (a, b) {
                var res = Common.compare(a, b);
                return res;
            });
            return array;
        };
        /**
         * 移除数组中的 无用对象
         * @param {Array} array
         * @returns {Array}
         */
        Collect.removeEmpty = function (array) {
            if (!Common.isArray(array))
                return null;
            var _array = array.filter(function (value, index) {
                if (Common.isEmpty(value))
                    return false;
                return true;
            });
            return _array;
        };
        /**
         *
         * @param {Array<Array<any>>} arraies
         * @param {Array<Number>} exclude
         * @returns {Array<Array<any>>}
         */
        Collect.removeColumnEmpty = function (arraies, exclude) {
            if (Common.isEmpty(arraies))
                return arraies;
            var emptyColumnCache = new HashMap();
            var item;
            for (var i = 0, lenI = arraies.length; i < lenI; i++) {
                if (exclude.indexOf(i) > -1)
                    continue;
                var array = arraies[i];
                if (Common.isEmpty(array))
                    continue;
                for (var j = 0, letJ = array.length; j < letJ; j++) {
                    if (!emptyColumnCache.has(j))
                        emptyColumnCache.push(j, true);
                    item = array[j];
                    if (!Common.isEmpty(item)) {
                        emptyColumnCache.push(j, false);
                    }
                }
            }
            var newArraies = new Array();
            var _loop_1 = function (i, lenI) {
                var array = arraies[i];
                var _array = new Array();
                emptyColumnCache.forEach(function (key, value) {
                    if (!value && (array.length > key))
                        _array.push(array[key]);
                });
                newArraies.push(_array);
            };
            for (var i = 0, lenI = arraies.length; i < lenI; i++) {
                _loop_1(i, lenI);
            }
            return newArraies;
        };
        Collect._removeColumnEmpty = function (arraies, exclude) {
            if (Common.isEmpty(arraies))
                return arraies;
            var emptyColumnCache = new HashMap();
            var item;
            for (var i = 0, lenI = arraies.length; i < lenI; i++) {
                if (exclude.indexOf(i) > -1)
                    continue;
                var array = arraies[i];
                if (Common.isEmpty(array))
                    continue;
                for (var j = 0, letJ = array.length; j < letJ; j++) {
                    if (!emptyColumnCache.has(j))
                        emptyColumnCache.push(j, true);
                    item = array[j];
                    if (!Common.isEmpty(item)) {
                        emptyColumnCache.push(j, false);
                    }
                }
            }
            emptyColumnCache.forEach(function (key, value) {
                if (!value)
                    return;
                for (var i = 0, lenI = arraies.length; i < lenI; i++) {
                    var array = arraies[i];
                    if (array.length > key)
                        array.splice(key, 1);
                }
            });
            return arraies;
        };
        /**
         *
         * @param {Array<any>} arraies
         * @param {Array<string>} keys
         * @param {number} defIndex
         * @returns {number}
         */
        Collect.getIndexByKey = function (arraies, keys, defIndex) {
            if (Common.isEmpty(arraies) || Common.isEmpty(keys))
                return defIndex;
            var item;
            var _loop_2 = function (i, len) {
                item = arraies[i];
                var ks = Common.convert(item, "string");
                var isThisRow = true;
                keys.forEach(function (key) {
                    if (ks.indexOf(key) == -1)
                        isThisRow = false;
                });
                if (isThisRow)
                    return { value: i };
            };
            for (var i = 0, len = arraies.length; i < len; i++) {
                var state_1 = _loop_2(i, len);
                if (typeof state_1 === "object")
                    return state_1.value;
            }
            return defIndex;
        };
        Collect.max = function (arraies, accuracy) {
            var keys = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                keys[_i - 2] = arguments[_i];
            }
            var res = this._statistics(arraies, accuracy, keys);
            return res["max"];
        };
        Collect.min = function (arraies, accuracy) {
            var keys = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                keys[_i - 2] = arguments[_i];
            }
            var res = this._statistics(arraies, accuracy, keys);
            return res["min"];
        };
        Collect.avg = function (arraies, accuracy) {
            var keys = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                keys[_i - 2] = arguments[_i];
            }
            var res = this._statistics(arraies, accuracy, keys);
            return res["avg"];
        };
        Collect.sum = function (arraies, accuracy) {
            var keys = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                keys[_i - 2] = arguments[_i];
            }
            var res = this._statistics(arraies, accuracy, keys);
            return res["sum"];
        };
        /**
         *
         * @param {Array<any>} arraies   要计算的数组
         * @param accuracy  小数点位数
         * @param {string} keys 要计算的字段
         * @returns {any}
         */
        Collect.statistics = function (arraies, accuracy) {
            var keys = [];
            for (var _i = 2; _i < arguments.length; _i++) {
                keys[_i - 2] = arguments[_i];
            }
            return this._statistics(arraies, accuracy, keys);
        };
        Collect._statistics = function (arraies, accuracy, keys) {
            var sum = 0;
            var count = 0;
            var max = Number.MIN_VALUE;
            var min = Number.MAX_VALUE;
            var result = { "count": 0, "sum": 0, "max": null, "min": null, "avg": null };
            if (!Common.isArray(arraies))
                return result;
            if (Common.isEmpty(keys)) {
                var item = void 0;
                for (var i = 0, len = arraies.length; i < len; i++) {
                    item = arraies[i];
                    var temp = Common.convert(item, "number");
                    if (!Common.isNumber(temp))
                        continue;
                    if (temp > max)
                        max = temp;
                    if (temp < min)
                        min = temp;
                    sum += temp;
                    count++;
                }
            }
            else {
                var item = void 0;
                for (var i = 0, lenI = arraies.length; i < lenI; i++) {
                    item = arraies[i];
                    var key = void 0;
                    for (var j = 0, lenJ = keys.length; j < lenJ; j++) {
                        key = keys[j];
                        var value = item[key];
                        var temp = Common.convert(value, "number");
                        if (!Common.isNumber(temp))
                            continue;
                        if (temp > max)
                            max = temp;
                        if (temp < min)
                            min = temp;
                        sum += temp;
                        count++;
                    }
                }
            }
            var _accuracy = Common.convert(accuracy, "number");
            if (count != 0) {
                var avg = sum / count;
                if (Common.isNumber(_accuracy)) {
                    sum = Common.convert(sum.toFixed(_accuracy), "number");
                    max = Common.convert(max.toFixed(_accuracy), "number");
                    min = Common.convert(min.toFixed(_accuracy), "number");
                    avg = Common.convert(avg.toFixed(_accuracy), "number");
                }
                result["count"] = count;
                result["sum"] = sum;
                result["max"] = max;
                result["min"] = min;
                result["avg"] = avg;
            }
            return result;
        };
        Collect.merge = function () {
            var arraies = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                arraies[_i] = arguments[_i];
            }
            var _array = [];
            if (Common.isEmpty(arraies) || !Common.isArray(arraies))
                return _array;
            var item;
            for (var i = 0, len = arraies.length; i < len; i++) {
                item = arraies[i];
                if (Common.isEmpty(item))
                    continue;
                if (Common.isArray(item)) {
                    _array = this._merge(item, _array);
                }
                else {
                    _array.push(item);
                }
            }
            return _array;
        };
        Collect._merge = function (arraies, res) {
            if (Common.isEmpty(arraies) || !Common.isArray(arraies))
                return res;
            var item;
            for (var i = 0, len = arraies.length; i < len; i++) {
                item = arraies[i];
                if (Common.isEmpty(item))
                    continue;
                if (Common.isArray(item)) {
                    res = this._merge(item, res);
                }
                else {
                    res.push(item);
                }
            }
            return res;
        };
        Collect.hasAllKeyWords = function (o, keyWords) {
            if (Common.isEmpty(o) || Common.isEmpty(keyWords))
                return false;
            var s = Common.convert(o, "string");
            var keyWord;
            for (var i = 0, len = keyWords.length; i < len; i++) {
                keyWord = Common.convert(keyWords[i], "string");
                if (s.indexOf(keyWord) === -1)
                    return false;
            }
            return true;
        };
        /**
         * 返回最新距离index
         * @returns {number}
         */
        Collect.minDistance = function (colIndex, value, arraies) {
            var array;
            var _value;
            var temp = Number.MAX_VALUE;
            var num = 0;
            for (var i = 0, len = arraies.length; i < len; i++) {
                array = arraies[i];
                if (Common.isEmpty(array) || !Common.isArray(array))
                    continue;
                _value = Common.convert(array[colIndex], 'number');
                var dis = Math.abs(_value - value);
                if (dis < temp) {
                    temp = dis;
                    num = i;
                }
            }
            return [num, temp];
        };
        Collect.distance = function (value1, value2) {
            return Math.abs(value1 - value2);
        };
        Collect.inMinDistance = function (value1, value2, threshold) {
            var distance = this.distance(value1, value2);
            return distance < threshold;
        };
        return Collect;
    }());
    ObjectUtil.Collect = Collect;
})(ObjectUtil = exports.ObjectUtil || (exports.ObjectUtil = {}));
/**
 * gis工具类
 */
var Gis;
(function (Gis) {
    var Common = ObjectUtil.Common;
    /**
     * 要素
     */
    var Feature = /** @class */ (function () {
        function Feature() {
        }
        return Feature;
    }());
    Gis.Feature = Feature;
    /**
     * 属性
     */
    var Property = /** @class */ (function () {
        function Property() {
        }
        return Property;
    }());
    Gis.Property = Property;
    /**
     * 图形
     */
    var Geometry = /** @class */ (function () {
        function Geometry() {
        }
        /**
         * 计算两个点之间的距离
         * @returns {number}
         */
        Geometry.distance2Point = function (x1, y1, x2, y2) {
            var _x1 = Common.convert(x1, "number");
            var _x2 = Common.convert(x2, "number");
            var _y1 = Common.convert(y1, "number");
            var _y2 = Common.convert(y2, "number");
            if (_x1 == null || _y1 == null || _x2 == null || _y2 == null)
                return null;
            var dx = _x1 - _x2;
            var dy = _y1 - _y2;
            var dis = Math.sqrt(dx * dx + dy * dy);
            return dis;
        };
        /**
         * 计算点到线段最近距离
         * @param {number} px
         * @param {number} py
         * @param {number} x1
         * @param {number} y1
         * @param {number} x2
         * @param y2
         * @returns {number}
         */
        Geometry.distance2Segment = function (px, py, x1, y1, x2, y2) {
            var space = 0;
            var a = this.distance2Point(x1, y1, x2, y2);
            var b = this.distance2Point(x1, y1, px, py);
            var c = this.distance2Point(x2, y2, px, py);
            if (c + b == a) {
                space = 0;
                return space;
            }
            if (a <= 0.000001) {
                space = b;
                return space;
            }
            if (c * c >= a * a + b * b) {
                space = b;
                return space;
            }
            if (b * b >= a * a + c * c) {
                space = c;
                return space;
            }
            var p = (a + b + c) / 2;
            var s = Math.sqrt(p * (p - a) * (p - b) * (p - c));
            space = 2 * s / a;
            return space;
        };
        /**
         * 判断点到坐标串最近距离
         * @param {number} px
         * @param {number} py
         * @param {Array<number[]>} line
         * @returns {number}
         */
        Geometry.distance2Line = function (px, py, line) {
            if (Common.isEmpty(line))
                return null;
            if (line.length == 1)
                return this.distance2Point(px, py, line[0][0], line[0][1]);
            var p1, p2;
            var dis = null;
            var min = Number.MAX_VALUE;
            for (var i = 0, len = line.length - 1; i < len; i++) {
                p1 = line[i];
                p2 = line[i + 1];
                dis = this.distance2Segment(px, py, p1[0], p1[1], p2[0], p2[1]);
                if (dis < min)
                    min = dis;
            }
            return min;
        };
        return Geometry;
    }());
    Gis.Geometry = Geometry;
})(Gis = exports.Gis || (exports.Gis = {}));
var Color = /** @class */ (function () {
    function Color() {
    }
    Color.seriesColor = function () {
        return ["#5470c6", "#91cc75", "#fac858", "#ee6666", "#73c0de", "#3ba272", "#fc8452", "#9a60b4", "#ea7ccc"];
    };
    Color.seriesColorMore = function (mum) {
        var colors = this.createColor(mum);
        return colors;
    };
    /**
     * max为整数
     * @param {number} max
     * @returns {string}
     */
    Color.createColor = function (max) {
        var colors = [];
        for (var i = 0; i < max; i++) {
            var color = this._randomColor(i / max);
            colors[i] = color;
        }
        return colors;
    };
    /**
     *
     * @param {number} seed 必须0到1
     * @returns {string}
     * @private
     */
    Color._randomColor = function (seed) {
        if (seed < 0)
            return '#FFFFFF';
        if (seed > 1)
            return '#000000';
        var letters = '0123456789ABCDEF';
        var color = '＃';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(seed * 16)];
        }
        return color;
    };
    Color.randomColor = function () {
        var letters = '0123456789ABCDEF';
        var color = '＃';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
    return Color;
}());
exports.Color = Color;
var Encrypt;
(function (Encrypt) {
    var Md5 = /** @class */ (function () {
        function Md5() {
            this._state = new Int32Array(4);
            this._buffer = new ArrayBuffer(68);
            this._buffer8 = new Uint8Array(this._buffer, 0, 68);
            this._buffer32 = new Uint32Array(this._buffer, 0, 17);
            this.start();
        }
        Md5.hashStr = function (str, raw) {
            if (raw === void 0) { raw = false; }
            return this.onePassHasher
                .start()
                .appendStr(str)
                .end(raw);
        };
        Md5.hashAsciiStr = function (str, raw) {
            if (raw === void 0) { raw = false; }
            return this.onePassHasher
                .start()
                .appendAsciiStr(str)
                .end(raw);
        };
        Md5._hex = function (x) {
            var hc = Md5.hexChars;
            var ho = Md5.hexOut;
            var n;
            var offset;
            var j;
            var i;
            for (i = 0; i < 4; i += 1) {
                offset = i * 8;
                n = x[i];
                for (j = 0; j < 8; j += 2) {
                    ho[offset + 1 + j] = hc.charAt(n & 0x0F);
                    n >>>= 4;
                    ho[offset + 0 + j] = hc.charAt(n & 0x0F);
                    n >>>= 4;
                }
            }
            return ho.join('');
        };
        Md5._md5cycle = function (x, k) {
            var a = x[0];
            var b = x[1];
            var c = x[2];
            var d = x[3];
            // ff()
            a += (b & c | ~b & d) + k[0] - 680876936 | 0;
            a = (a << 7 | a >>> 25) + b | 0;
            d += (a & b | ~a & c) + k[1] - 389564586 | 0;
            d = (d << 12 | d >>> 20) + a | 0;
            c += (d & a | ~d & b) + k[2] + 606105819 | 0;
            c = (c << 17 | c >>> 15) + d | 0;
            b += (c & d | ~c & a) + k[3] - 1044525330 | 0;
            b = (b << 22 | b >>> 10) + c | 0;
            a += (b & c | ~b & d) + k[4] - 176418897 | 0;
            a = (a << 7 | a >>> 25) + b | 0;
            d += (a & b | ~a & c) + k[5] + 1200080426 | 0;
            d = (d << 12 | d >>> 20) + a | 0;
            c += (d & a | ~d & b) + k[6] - 1473231341 | 0;
            c = (c << 17 | c >>> 15) + d | 0;
            b += (c & d | ~c & a) + k[7] - 45705983 | 0;
            b = (b << 22 | b >>> 10) + c | 0;
            a += (b & c | ~b & d) + k[8] + 1770035416 | 0;
            a = (a << 7 | a >>> 25) + b | 0;
            d += (a & b | ~a & c) + k[9] - 1958414417 | 0;
            d = (d << 12 | d >>> 20) + a | 0;
            c += (d & a | ~d & b) + k[10] - 42063 | 0;
            c = (c << 17 | c >>> 15) + d | 0;
            b += (c & d | ~c & a) + k[11] - 1990404162 | 0;
            b = (b << 22 | b >>> 10) + c | 0;
            a += (b & c | ~b & d) + k[12] + 1804603682 | 0;
            a = (a << 7 | a >>> 25) + b | 0;
            d += (a & b | ~a & c) + k[13] - 40341101 | 0;
            d = (d << 12 | d >>> 20) + a | 0;
            c += (d & a | ~d & b) + k[14] - 1502002290 | 0;
            c = (c << 17 | c >>> 15) + d | 0;
            b += (c & d | ~c & a) + k[15] + 1236535329 | 0;
            b = (b << 22 | b >>> 10) + c | 0;
            // gg()
            a += (b & d | c & ~d) + k[1] - 165796510 | 0;
            a = (a << 5 | a >>> 27) + b | 0;
            d += (a & c | b & ~c) + k[6] - 1069501632 | 0;
            d = (d << 9 | d >>> 23) + a | 0;
            c += (d & b | a & ~b) + k[11] + 643717713 | 0;
            c = (c << 14 | c >>> 18) + d | 0;
            b += (c & a | d & ~a) + k[0] - 373897302 | 0;
            b = (b << 20 | b >>> 12) + c | 0;
            a += (b & d | c & ~d) + k[5] - 701558691 | 0;
            a = (a << 5 | a >>> 27) + b | 0;
            d += (a & c | b & ~c) + k[10] + 38016083 | 0;
            d = (d << 9 | d >>> 23) + a | 0;
            c += (d & b | a & ~b) + k[15] - 660478335 | 0;
            c = (c << 14 | c >>> 18) + d | 0;
            b += (c & a | d & ~a) + k[4] - 405537848 | 0;
            b = (b << 20 | b >>> 12) + c | 0;
            a += (b & d | c & ~d) + k[9] + 568446438 | 0;
            a = (a << 5 | a >>> 27) + b | 0;
            d += (a & c | b & ~c) + k[14] - 1019803690 | 0;
            d = (d << 9 | d >>> 23) + a | 0;
            c += (d & b | a & ~b) + k[3] - 187363961 | 0;
            c = (c << 14 | c >>> 18) + d | 0;
            b += (c & a | d & ~a) + k[8] + 1163531501 | 0;
            b = (b << 20 | b >>> 12) + c | 0;
            a += (b & d | c & ~d) + k[13] - 1444681467 | 0;
            a = (a << 5 | a >>> 27) + b | 0;
            d += (a & c | b & ~c) + k[2] - 51403784 | 0;
            d = (d << 9 | d >>> 23) + a | 0;
            c += (d & b | a & ~b) + k[7] + 1735328473 | 0;
            c = (c << 14 | c >>> 18) + d | 0;
            b += (c & a | d & ~a) + k[12] - 1926607734 | 0;
            b = (b << 20 | b >>> 12) + c | 0;
            // hh()
            a += (b ^ c ^ d) + k[5] - 378558 | 0;
            a = (a << 4 | a >>> 28) + b | 0;
            d += (a ^ b ^ c) + k[8] - 2022574463 | 0;
            d = (d << 11 | d >>> 21) + a | 0;
            c += (d ^ a ^ b) + k[11] + 1839030562 | 0;
            c = (c << 16 | c >>> 16) + d | 0;
            b += (c ^ d ^ a) + k[14] - 35309556 | 0;
            b = (b << 23 | b >>> 9) + c | 0;
            a += (b ^ c ^ d) + k[1] - 1530992060 | 0;
            a = (a << 4 | a >>> 28) + b | 0;
            d += (a ^ b ^ c) + k[4] + 1272893353 | 0;
            d = (d << 11 | d >>> 21) + a | 0;
            c += (d ^ a ^ b) + k[7] - 155497632 | 0;
            c = (c << 16 | c >>> 16) + d | 0;
            b += (c ^ d ^ a) + k[10] - 1094730640 | 0;
            b = (b << 23 | b >>> 9) + c | 0;
            a += (b ^ c ^ d) + k[13] + 681279174 | 0;
            a = (a << 4 | a >>> 28) + b | 0;
            d += (a ^ b ^ c) + k[0] - 358537222 | 0;
            d = (d << 11 | d >>> 21) + a | 0;
            c += (d ^ a ^ b) + k[3] - 722521979 | 0;
            c = (c << 16 | c >>> 16) + d | 0;
            b += (c ^ d ^ a) + k[6] + 76029189 | 0;
            b = (b << 23 | b >>> 9) + c | 0;
            a += (b ^ c ^ d) + k[9] - 640364487 | 0;
            a = (a << 4 | a >>> 28) + b | 0;
            d += (a ^ b ^ c) + k[12] - 421815835 | 0;
            d = (d << 11 | d >>> 21) + a | 0;
            c += (d ^ a ^ b) + k[15] + 530742520 | 0;
            c = (c << 16 | c >>> 16) + d | 0;
            b += (c ^ d ^ a) + k[2] - 995338651 | 0;
            b = (b << 23 | b >>> 9) + c | 0;
            // ii()
            a += (c ^ (b | ~d)) + k[0] - 198630844 | 0;
            a = (a << 6 | a >>> 26) + b | 0;
            d += (b ^ (a | ~c)) + k[7] + 1126891415 | 0;
            d = (d << 10 | d >>> 22) + a | 0;
            c += (a ^ (d | ~b)) + k[14] - 1416354905 | 0;
            c = (c << 15 | c >>> 17) + d | 0;
            b += (d ^ (c | ~a)) + k[5] - 57434055 | 0;
            b = (b << 21 | b >>> 11) + c | 0;
            a += (c ^ (b | ~d)) + k[12] + 1700485571 | 0;
            a = (a << 6 | a >>> 26) + b | 0;
            d += (b ^ (a | ~c)) + k[3] - 1894986606 | 0;
            d = (d << 10 | d >>> 22) + a | 0;
            c += (a ^ (d | ~b)) + k[10] - 1051523 | 0;
            c = (c << 15 | c >>> 17) + d | 0;
            b += (d ^ (c | ~a)) + k[1] - 2054922799 | 0;
            b = (b << 21 | b >>> 11) + c | 0;
            a += (c ^ (b | ~d)) + k[8] + 1873313359 | 0;
            a = (a << 6 | a >>> 26) + b | 0;
            d += (b ^ (a | ~c)) + k[15] - 30611744 | 0;
            d = (d << 10 | d >>> 22) + a | 0;
            c += (a ^ (d | ~b)) + k[6] - 1560198380 | 0;
            c = (c << 15 | c >>> 17) + d | 0;
            b += (d ^ (c | ~a)) + k[13] + 1309151649 | 0;
            b = (b << 21 | b >>> 11) + c | 0;
            a += (c ^ (b | ~d)) + k[4] - 145523070 | 0;
            a = (a << 6 | a >>> 26) + b | 0;
            d += (b ^ (a | ~c)) + k[11] - 1120210379 | 0;
            d = (d << 10 | d >>> 22) + a | 0;
            c += (a ^ (d | ~b)) + k[2] + 718787259 | 0;
            c = (c << 15 | c >>> 17) + d | 0;
            b += (d ^ (c | ~a)) + k[9] - 343485551 | 0;
            b = (b << 21 | b >>> 11) + c | 0;
            x[0] = a + x[0] | 0;
            x[1] = b + x[1] | 0;
            x[2] = c + x[2] | 0;
            x[3] = d + x[3] | 0;
        };
        Md5.prototype.start = function () {
            this._dataLength = 0;
            this._bufferLength = 0;
            this._state.set(Md5.stateIdentity);
            return this;
        };
        // Char to code point to to array conversion:
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/charCodeAt
        // #Example.3A_Fixing_charCodeAt_to_handle_non-Basic-Multilingual-Plane_characters_if_their_presence_earlier_in_the_string_is_unknown
        Md5.prototype.appendStr = function (str) {
            var buf8 = this._buffer8;
            var buf32 = this._buffer32;
            var bufLen = this._bufferLength;
            var code;
            var i;
            for (i = 0; i < str.length; i += 1) {
                code = str.charCodeAt(i);
                if (code < 128) {
                    buf8[bufLen++] = code;
                }
                else if (code < 0x800) {
                    buf8[bufLen++] = (code >>> 6) + 0xC0;
                    buf8[bufLen++] = code & 0x3F | 0x80;
                }
                else if (code < 0xD800 || code > 0xDBFF) {
                    buf8[bufLen++] = (code >>> 12) + 0xE0;
                    buf8[bufLen++] = (code >>> 6 & 0x3F) | 0x80;
                    buf8[bufLen++] = (code & 0x3F) | 0x80;
                }
                else {
                    code = ((code - 0xD800) * 0x400) + (str.charCodeAt(++i) - 0xDC00) + 0x10000;
                    if (code > 0x10FFFF) {
                        throw new Error('Unicode standard supports code points up to U+10FFFF');
                    }
                    buf8[bufLen++] = (code >>> 18) + 0xF0;
                    buf8[bufLen++] = (code >>> 12 & 0x3F) | 0x80;
                    buf8[bufLen++] = (code >>> 6 & 0x3F) | 0x80;
                    buf8[bufLen++] = (code & 0x3F) | 0x80;
                }
                if (bufLen >= 64) {
                    this._dataLength += 64;
                    Md5._md5cycle(this._state, buf32);
                    bufLen -= 64;
                    buf32[0] = buf32[16];
                }
            }
            this._bufferLength = bufLen;
            return this;
        };
        Md5.prototype.appendAsciiStr = function (str) {
            var buf8 = this._buffer8;
            var buf32 = this._buffer32;
            var bufLen = this._bufferLength;
            var i;
            var j = 0;
            for (;;) {
                i = Math.min(str.length - j, 64 - bufLen);
                while (i--) {
                    buf8[bufLen++] = str.charCodeAt(j++);
                }
                if (bufLen < 64) {
                    break;
                }
                this._dataLength += 64;
                Md5._md5cycle(this._state, buf32);
                bufLen = 0;
            }
            this._bufferLength = bufLen;
            return this;
        };
        Md5.prototype.appendByteArray = function (input) {
            var buf8 = this._buffer8;
            var buf32 = this._buffer32;
            var bufLen = this._bufferLength;
            var i;
            var j = 0;
            for (;;) {
                i = Math.min(input.length - j, 64 - bufLen);
                while (i--) {
                    buf8[bufLen++] = input[j++];
                }
                if (bufLen < 64) {
                    break;
                }
                this._dataLength += 64;
                Md5._md5cycle(this._state, buf32);
                bufLen = 0;
            }
            this._bufferLength = bufLen;
            return this;
        };
        Md5.prototype.getState = function () {
            var self = this;
            var s = self._state;
            return {
                buffer: String.fromCharCode.apply(null, self._buffer8),
                buflen: self._bufferLength,
                length: self._dataLength,
                state: [s[0], s[1], s[2], s[3]]
            };
        };
        Md5.prototype.setState = function (state) {
            var buf = state.buffer;
            var x = state.state;
            var s = this._state;
            var i;
            this._dataLength = state.length;
            this._bufferLength = state.buflen;
            s[0] = x[0];
            s[1] = x[1];
            s[2] = x[2];
            s[3] = x[3];
            for (i = 0; i < buf.length; i += 1) {
                this._buffer8[i] = buf.charCodeAt(i);
            }
        };
        Md5.prototype.end = function (raw) {
            if (raw === void 0) { raw = false; }
            var bufLen = this._bufferLength;
            var buf8 = this._buffer8;
            var buf32 = this._buffer32;
            var i = (bufLen >> 2) + 1;
            var dataBitsLen;
            this._dataLength += bufLen;
            buf8[bufLen] = 0x80;
            buf8[bufLen + 1] = buf8[bufLen + 2] = buf8[bufLen + 3] = 0;
            buf32.set(Md5.buffer32Identity.subarray(i), i);
            if (bufLen > 55) {
                Md5._md5cycle(this._state, buf32);
                buf32.set(Md5.buffer32Identity);
            }
            // Do the final computation based on the tail and length
            // Beware that the final length may not fit in 32 bits so we take care of that
            dataBitsLen = this._dataLength * 8;
            if (dataBitsLen <= 0xFFFFFFFF) {
                buf32[14] = dataBitsLen;
            }
            else {
                var matches = dataBitsLen.toString(16).match(/(.*?)(.{0,8})$/);
                if (matches === null) {
                    return;
                }
                var lo = parseInt(matches[2], 16);
                var hi = parseInt(matches[1], 16) || 0;
                buf32[14] = lo;
                buf32[15] = hi;
            }
            Md5._md5cycle(this._state, buf32);
            return raw ? this._state : Md5._hex(this._state);
        };
        // Private Static Variables
        Md5.stateIdentity = new Int32Array([1732584193, -271733879, -1732584194, 271733878]);
        Md5.buffer32Identity = new Int32Array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        Md5.hexChars = '0123456789abcdef';
        Md5.hexOut = [];
        // Permanent instance is to use for one-call hashing
        Md5.onePassHasher = new Md5();
        return Md5;
    }());
    Encrypt.Md5 = Md5;
    if (Md5.hashStr('hello') !== '5d41402abc4b2a76b9719d911017c592') {
        console.error('Md5 self test failed.');
    }
})(Encrypt = exports.Encrypt || (exports.Encrypt = {}));
/**
 * 事件注册与通知
 */
var Et;
(function (Et) {
    var HashMap = Util.Map.HashMap;
    var Manager = /** @class */ (function () {
        function Manager() {
        }
        Manager.register = function (eventName, callback) {
            if (ObjectUtil.Common.isEmpty(eventName))
                throw new Error("请输入要注册的方法名称！");
            //if (this.iEvents.has(eventName)) throw new Error("该方法已被注册！" + eventName);
            this.iEvents.push(eventName, { callback: callback });
            return this;
        };
        Manager.unregister = function (eventName) {
            this.iEvents.remove(eventName);
            return this;
        };
        Manager.registerAll = function () {
            this.iEvents.clear();
            return this;
        };
        Manager.registerBc = function (eventName, flag, callback) {
            if (ObjectUtil.Common.isEmpty(eventName))
                console.error("请输入要注册的方法名称！");
            if (!this.iEventBc.has(eventName))
                this.iEventBc.push(eventName, new HashMap());
            this.iEventBc.get(eventName).push(flag, { callback: callback });
            return this;
        };
        Manager.unregisterBc = function (eventName, flag) {
            if (ObjectUtil.Common.isEmpty(eventName)) {
                console.error("请输入要注册的方法名称！");
                return;
            }
            var iEvents = this.iEventBc.get(eventName);
            if (iEvents == null)
                return;
            iEvents.remove(flag);
            return this;
        };
        Manager.registerBcAll = function () {
            this.iEventBc.clear();
            return this;
        };
        Manager.release = function () {
            this.iEvents.clear();
            this.iEventBc.clear();
            return this;
        };
        /**
         * 执行方法
         * @param {string} eventName
         * @param data
         * @returns {any}
         */
        Manager.dispatch = function (eventName) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            if (!this.iEvents.has(eventName)) {
                console.error("未能找到要执行的方法！" + eventName);
                return this;
            }
            var event = this.iEvents.get(eventName);
            if (event && event.callback) {
                var cmd = this.executeCmd(params);
                var data = eval(cmd);
                return data;
            }
        };
        Manager.executeCmd = function (params) {
            var _args = "";
            for (var i = 0, len = params.length; i < len; i++) {
                var arg = "params[" + i + "]";
                _args += arg;
                if (len >= 2 && i != len - 1)
                    _args += ",";
            }
            var exe = "event.callback(" + _args + ")";
            return exe;
        };
        /**
         * 广播
         * @param {string} eventName
         * @param params
         * @returns {any}
         */
        Manager.dispatchBc = function (eventName) {
            var params = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                params[_i - 1] = arguments[_i];
            }
            if (!this.iEventBc.has(eventName)) {
                console.error("未能找到要执行的广播！" + eventName);
                return this;
            }
            var events = this.iEventBc.get(eventName);
            var that = this;
            events.forEach(function (key, event) {
                if (event && event.callback) {
                    var cmd = that.executeCmd(params);
                    eval(cmd);
                }
            });
        };
        /**
         * 1 vs 1 事件
         * @type {Util.Map.HashMap<any, any>}
         */
        Manager.iEvents = new HashMap();
        /**
         * 1 vs 多  广播
         * @type {Util.Map.HashMap<any, any>}
         */
        Manager.iEventBc = new HashMap();
        return Manager;
    }());
    Et.Manager = Manager;
})(Et = exports.Et || (exports.Et = {}));
function main() {
    var a = ObjectUtil.Collect.removeColumnEmpty([["a", "b", "c", "d"], [1, 1, 3, 1, 2], [1, null, 3, 4], [1, null, 3]], [0]);
    var b = ObjectUtil.Collect.sort([113, "z", "b", "c", "d", "1", "2", "6", "0", "12", "2020-01-02 12:13:24", "2020-01-02 12:13:23", "2020-02-02 12:13:23", "2020-01-03 12:13:23"]);
    b = ObjectUtil.Collect.sort(["111", "33", "2020-01-02 12:13:24", "2020-01-02 12:13:23", "2020-02-02 12:13:23", "2020-01-03 12:13:23"]);
    alert(ObjectUtil.Common.convert(new Date().getTime(), "date"));
}
