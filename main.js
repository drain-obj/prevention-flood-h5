import Vue from 'vue'
import App from './App'
import uView from './components/uview-ui';
Vue.use(uView);
import api from "./https";
Vue.use(api);

// vuex
import store from './store'
import "./assets/css/common.scss";
Vue.prototype.defaultPost = "http://"+ window.location.hostname +":7171"; //开发环境图片地址
Vue.prototype.wbUrl = "ws://"+ window.location.hostname +":1222";  //wk
// Vue.prototype.wbUrl = "ws://10.5.4.80:1222";  //wk
// Vue.prototype.defaultPost = "http://10.5.4.80:7171"; //开发环境图片地址
Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App,
	store
})
app.$mount()
